﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models
{
    public class ResultModel
    {
        public string Message { get; set; }
        public DateTime Date { get; set; }

        public override string ToString()
        {
            return $"Message: {Message}, Date: {Date}";
        }
    }
}
