﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models
{
    public class Project
    {
        public int id;
        public string name;
        public string description;
        public DateTime created_at;
        public DateTime deadline;
        public int author_id;
        public int team_id;

        public override string ToString()
        {
            return $"Id: {id}, Name: {name}, Description: {description.Replace("\n", " ")}, Create at: {created_at}, Deadline: {deadline}, " +
                $"Author id: {author_id}, Team id: {team_id}\n";
        }
    }
}
