﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Models.LinqModels
{
    public class UserInformationViewModel
    {
        public UserViewModel User { get; set; }
        public ProjectViewModel MinProject { get; set; }
        public int CountOfTaskInMinProject { get; set; }
        public int CountOfCanceledOrNotFinishedTasks { get; set; }
        public TasksViewModel LongestTask { get; set; }
    }
}
