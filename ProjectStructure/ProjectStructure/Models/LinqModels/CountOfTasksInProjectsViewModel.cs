﻿using ProjectStructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Models.LinqModels
{
    public class CountOfTasksInProjectsViewModel
    {
        public ProjectViewModel Project { get; set; }
        public int CountOfTasks { get; set; }
    }
}
