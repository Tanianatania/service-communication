﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Models.LinqModels
{
    public class ProjectInformationViewModel
    {
        public ProjectViewModel Project { get; set; }
        public TasksViewModel TaskWithLongestDescrition { get; set; }
        public TasksViewModel TaskWithShortestName { get; set; }
        public int CountOfUser { get; set; }
    }
}
