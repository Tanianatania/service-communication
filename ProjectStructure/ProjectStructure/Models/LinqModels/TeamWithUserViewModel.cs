﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Models.LinqModels
{
    public class TeamWithUserViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserViewModel> Users { get; set; }

    }
}
