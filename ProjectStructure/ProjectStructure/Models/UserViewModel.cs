﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime Registered_at { get; set; }
        public int? Team_id { get; set; }

        public override string ToString()
        {
            return $" First name: {First_name}, Last name: {Last_name}, Email: {Email}, Birthday: {Birthday}," +
                $"Registered at: {Registered_at}, Team id: {Team_id} ";
        }
    }
}
