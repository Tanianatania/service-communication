﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using ProjectStructure.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IService<UserDTO> _service;
        private readonly IMapper _mapper;

        public UsersController(IService<UserDTO> service,
            IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: api/Users
        [HttpGet]
        public IEnumerable<UserViewModel> Get()
        {
            return _service.GetList().Select(item => _mapper.Map<UserDTO, UserViewModel>(item));
        }

        // GET: api/Users/5
        [HttpGet("{id}", Name = "GetUser")]
        public UserViewModel Get(int id)
        {
            return _mapper.Map<UserDTO, UserViewModel>(_service.Get(id));
        }

        // POST: api/Users
        [HttpPost]
        public void Post([FromBody] string value)
        {
            UserViewModel item = JsonConvert.DeserializeObject<UserViewModel>(value);
            _service.Create(_mapper.Map<UserViewModel, UserDTO>(item));
        }

        // PUT: api/Users/5
        [HttpPut]
        public void Put( [FromBody] string value)
        {
            UserViewModel item = JsonConvert.DeserializeObject<UserViewModel>(value);
            _service.Update(_mapper.Map<UserViewModel, UserDTO>(item));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
