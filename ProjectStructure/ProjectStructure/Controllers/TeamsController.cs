﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.Models;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IService<TeamDTO> _service;
        private readonly IMapper _mapper;

        public TeamsController(IService<TeamDTO> service,
            IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: api/Teams
        [HttpGet]
        public IEnumerable<TeamViewModel> Get()
        {
            return _service.GetList().Select(item => _mapper.Map<TeamDTO, TeamViewModel>(item));
        }

        // GET: api/Teams/5
        [HttpGet("{id}", Name = "GetTeam")]
        public TeamViewModel Get(int id)
        {
            return _mapper.Map<TeamDTO, TeamViewModel>(_service.Get(id));
        }

        // POST: api/Teams
        [HttpPost]
        public void Post([FromBody] string value)
        {
            TeamViewModel item = JsonConvert.DeserializeObject<TeamViewModel>(value);
            _service.Create(_mapper.Map<TeamViewModel, TeamDTO>(item));
        }

        // PUT: api/Teams
        [HttpPut]
        public void Put([FromBody] string value)
        {
            TeamViewModel item = JsonConvert.DeserializeObject<TeamViewModel>(value);
            _service.Update(_mapper.Map<TeamViewModel, TeamDTO>(item));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
