﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.Models;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStateModelsController : ControllerBase
    {
        private readonly IService<TaskStateModelDTO> _service;
        private readonly IMapper _mapper;

        public TaskStateModelsController(IService<TaskStateModelDTO> service,
            IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: api/TaskStateModels
        [HttpGet]
        public IEnumerable<TaskStateViewModel> Get()
        {
            return _service.GetList().Select(item => _mapper.Map<TaskStateModelDTO, TaskStateViewModel>(item));
        }

        // GET: api/TaskStateModels/5
        [HttpGet("{id}", Name = "GetTaskStateModel")]
        public TaskStateViewModel Get(int id)
        {
            return _mapper.Map<TaskStateModelDTO, TaskStateViewModel>(_service.Get(id));
        }

        // POST: api/TaskStateModels
        [HttpPost]
        public void Post([FromBody] string value)
        {
            TaskStateViewModel item = JsonConvert.DeserializeObject<TaskStateViewModel>(value);
            _service.Create(_mapper.Map<TaskStateViewModel, TaskStateModelDTO>(item));
        }

        // PUT: api/TaskStateModels
        [HttpPut]
        public void Put([FromBody] string value)
        {
            TaskStateViewModel item = JsonConvert.DeserializeObject<TaskStateViewModel>(value);
            _service.Update(_mapper.Map<TaskStateViewModel, TaskStateModelDTO>(item));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
