﻿using DAL.Entities;
using System;
using System.Collections.Generic;

namespace DAL
{
    public class DataSource
    {

        public List<Project> GetProjects()
        {
            List<Project> projects = new List<Project>();
            projects.Add(new Project
            {
                Id =1,
                Name = "Project1",
                Deadline = DateTime.Now.AddDays(19),
                Author_id = 1,
                Description = "It`s project 1",
                Created_at = DateTime.Now,
                Team_id = 1
            });

            projects.Add(new Project
            {
                Id = 2,
                Name = "Project2",
                Deadline = DateTime.Now.AddDays(190),
                Author_id = 2,
                Description = "It`s project 2",
                Created_at = DateTime.Now.AddDays(-12),
                Team_id = 2
            });

            return projects;
        }

        public List<Tasks> GetTasks()
        {
            List<Tasks> tasks = new List<Tasks>();
            tasks.Add(new Tasks
            {
                Id = 1,
                Name = "Task 1",
                Created_at = DateTime.Now,
                Finished_at = DateTime.Now.AddMonths(10),
                Description = "First Task",
                Project_id = 1,
                Performer_id = 2,
                State = 3
            });

            tasks.Add(new Tasks
            {
                Id = 2,
                Name = "Task 2",
                Created_at = DateTime.Now,
                Finished_at = DateTime.Now,
                Description = "Second Task",
                Project_id = 2,
                Performer_id = 1,
                State = 3
            });

            return tasks;
        }

        public List<Team> GetTeams()
        {
            List<Team> teams = new List<Team>();
            teams.Add(new Team
            {
                Name = "Tania",
                Created_at = DateTime.Now,
                Id = 1
            });
            teams.Add(new Team
            {
                Name = "Rass",
                Created_at = DateTime.Now,
                Id = 2
            });


            return teams;
        }

        public List<User> GetUsers()
        {
            List<User> users = new List<User>();
            users.Add(new User
            {
                Id = 1,
                First_name = "Tania",
                Last_name = "Hutii",
                Team_id = 1,
                Birthday = DateTime.Now.AddYears(-13),
                Email = "gsgsgsgs",
                Registered_at = DateTime.Now
            });
            users.Add(new User
            {
                Id = 2,
                First_name = "Rass",
                Last_name = "Hutii",
                Team_id = 2,
                Birthday = DateTime.Now.AddYears(-16),
                Email = "gsgsgsgs",
                Registered_at = DateTime.Now
            });


            return users;
        }

        public List<TaskStateModel> GetAllTaskStateModels()
        {
            List<TaskStateModel> taskStates = new List<TaskStateModel>();
            taskStates.Add(new TaskStateModel
            {
                Id = 0,
                Value= "Created"
            });
            taskStates.Add(new TaskStateModel
            {
                Id = 1,
                Value = "Started"
            });
            taskStates.Add(new TaskStateModel
            {
                Id = 2,
                Value = "Finished"
            });
            taskStates.Add(new TaskStateModel
            {
                Id = 3,
                Value = "Canceled"
            });

            return taskStates;
        }
    }
}
