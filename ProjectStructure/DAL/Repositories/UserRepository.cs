﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly List<User> _users;
        DataSource dataSource = new DataSource();

        public UserRepository()
        {
            _users = dataSource.GetUsers();
        }

        public IEnumerable<User> GetList()
        {
            return _users;
        }

        public User Get(int id)
        {
            return _users.Where(a => a.Id == id).FirstOrDefault();
        }

        public void Create(User item)
        {
            if (_users.Count != 0)
            {
                var _lastProject = _users.Last();
                item.Id = _lastProject.Id + 1;
            }
            _users.Add(item);
        }

        public void Update(User item)
        {
            var oldTasks = _users.Where(a => a.Id == item.Id).FirstOrDefault();
            if (oldTasks != null)
            {
                _users.Remove(oldTasks);
                _users.Add(item);
            }
        }

        public void Delete(int id)
        {
            var oldTasks = _users.Where(a => a.Id == id).FirstOrDefault();
            if (oldTasks != null)
            {
                _users.Remove(oldTasks);
            }
        }
    }
}
