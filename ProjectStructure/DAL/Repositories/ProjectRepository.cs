﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.Repositories
{
   public class ProjectRepository : IRepository<Project>
    {
        private readonly List<Project> _projects;
        DataSource dataSource = new DataSource();

        public ProjectRepository()
        {
            _projects = dataSource.GetProjects();
        }

        public IEnumerable<Project> GetList()
        {
            return _projects;
        }

        public Project Get(int id)
        {
            return _projects.Where(a => a.Id == id).FirstOrDefault();
        }

        public void Create(Project item)
        {
            if (_projects.Count!=0)
            {
                var _lastProject = _projects.Last();
                item.Id = _lastProject.Id + 1;
            }          
            _projects.Add(item);
        }

        public void Update(Project item)
        {
            var oldProject = _projects.Where(a => a.Id == item.Id).FirstOrDefault();
            if (oldProject != null)
            {
                var old=_projects.Remove(oldProject);
                _projects.Add(item);
            }
        }

        public void Delete(int id)
        {
            var oldProject = _projects.Where(a => a.Id == id).FirstOrDefault();
            if (oldProject != null)
            {
                _projects.Remove(oldProject);
            }
        }
    }
}
