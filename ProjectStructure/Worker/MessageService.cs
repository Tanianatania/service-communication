﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SharedQueueService.Interfaces;
using SharedQueueService.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Worker.Models;

namespace Worker
{
    public class MessageService : IDisposable
    {
        IMessageProducerScope _messageProducerScope;
        IMessageConsumerScope _messageConsumerScope;

        public MessageService(
            IMessageProducerScopeFactory messageProducerScopeFactory,
             IMessageConsumerScopeFactory messageConsumerScopeFactory)
        {
            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSetting
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "SendValueQueue",
                RoutingKey = "*.queue.#"
            });

            _messageConsumerScope.MessageConsumer.Received += MessageReceived;

            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSetting
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponseQueue",
                RoutingKey = "response"
            });
        }


        private void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var procedder = false;
            try
            {
                var value = Encoding.UTF8.GetString(args.Body);
                ResultModel result = new ResultModel
                {
                    Message = value,
                    Date = DateTime.Now
                };
                var jsonResult = JsonConvert.SerializeObject(result);
                using(StreamWriter sw=new StreamWriter("../../../Result.txt",true))
                {
                    sw.WriteLine(jsonResult);
                }
                SendSuccessfulState();
                procedder = true;
            }
            catch (Exception e)
            {
                SendFailedState(e.Message);
                procedder = false;
            }
            finally
            {
                _messageConsumerScope.MessageConsumer.SetActnowledge(args.DeliveryTag, procedder);
            }
        }

        public void SendSuccessfulState()
        {
            _messageProducerScope.MessageProducer.Send("Successfully received");
        }

        public void SendFailedState(string errorMessage)
        {
            _messageProducerScope.MessageProducer.Send($"Failed during handling a message: {errorMessage}");
        }

        public void Dispose()
        {
            _messageProducerScope?.Dispose();
            _messageConsumerScope?.Dispose();
        }
    }
}

