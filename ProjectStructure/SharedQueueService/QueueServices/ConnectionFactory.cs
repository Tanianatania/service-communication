﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharedQueueService.QueueServices
{
    public class ConnectionFactory:RabbitMQ.Client.ConnectionFactory
    {
        public ConnectionFactory(Uri uri)
        {
            HostName = "127.0.0.1:5673";
            Uri = uri;
            RequestedConnectionTimeout = 30000;
            NetworkRecoveryInterval = TimeSpan.FromSeconds(30);
            AutomaticRecoveryEnabled = true;
            TopologyRecoveryEnabled = true;
            RequestedHeartbeat = 60;
        }
    }
}
