﻿using RabbitMQ.Client;
using SharedQueueService.Interfaces;
using SharedQueueService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharedQueueService.QueueServices
{
    public class MessageConsumerScope: IMessageConsumerScope
    {
        private readonly MessageScopeSetting _messageScopeSetting;
        private readonly Lazy<IMessageQueue> _messageQueueLazy;
        private readonly Lazy<IMessageConsumer> _messageConsumerLazy;

        private readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScope(IConnectionFactory connectionFactory, MessageScopeSetting messageScopeSetting)
        {
            _connectionFactory = connectionFactory;
            _messageScopeSetting = messageScopeSetting;

            _messageQueueLazy = new Lazy<IMessageQueue>(CreateMessageQueue);
            _messageConsumerLazy = new Lazy<IMessageConsumer>(CreateMessangeConsumer);
        }

        public IMessageConsumer MessageConsumer => _messageConsumerLazy.Value;
        private IMessageQueue MessageQueue => _messageQueueLazy.Value;

        private IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _messageScopeSetting);
        }



        private IMessageConsumer CreateMessangeConsumer()
        {
            return new MessageConsumer(new MessageConsumerSetting
            {
                Channel = MessageQueue.Channel,
                QueueName=_messageScopeSetting.QueueName
            });
        }

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }

    }
}
