﻿using SharedQueueService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharedQueueService.Interfaces
{
    public interface IMessageConsumerScopeFactory
    {
        IMessageConsumerScope Open(MessageScopeSetting messageScopeSetting);
        IMessageConsumerScope Connect(MessageScopeSetting messageScopeSetting);

    }
}
