﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO.LinqModels
{
    public class ProjectInformationDTO
    {
        public ProjectDTO Project { get; set; }
        public TasksDTO TaskWithLongestDescrition { get; set; }
        public TasksDTO TaskWithShortestName { get; set; }
        public int CountOfUser { get; set; }
    }
}
