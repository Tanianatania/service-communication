﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class TaskStateModelDTO
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
