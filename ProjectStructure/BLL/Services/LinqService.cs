﻿using AutoMapper;
using BLL.DTO;
using BLL.DTO.LinqModels;
using BLL.Hubs;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SharedQueueService.Interfaces;
using SharedQueueService.Models;
using SharedQueueService.QueueServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class LinqService : ILinqService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Tasks> _taskRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Team> _teamRepository;
        IMessageProducerScope _messageProducerScope;
        IMessageConsumerScope _messageConsumerScope;
        private readonly IHubContext<ProjectHub> _projectHub;
        private List<TasksDTO> _tasks;
        private List<ProjectDTO> _projects;
        private List<UserDTO> _users;
        private List<TeamDTO> _teams;

        public LinqService(IRepository<Project> projectRepository,
            IRepository<Tasks> taskRepository,
            IRepository<User> userRepository,
            IRepository<Team> teamRepository,
        IMessageProducerScopeFactory messageProducerScopeFactory,
             IMessageConsumerScopeFactory messageConsumerScopeFactory,
             IHubContext<ProjectHub> projectHub,
            IMapper mapper)
        {
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSetting
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "SendValueQueue",
                RoutingKey = "topic.queue"
            });

            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSetting
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponseQueue",
                RoutingKey = "response"
            });
            _messageConsumerScope.MessageConsumer.Received += GetValue;

            _projectHub = projectHub;
            _projectRepository = projectRepository;
            _taskRepository = taskRepository;
            _userRepository = userRepository;
            _teamRepository = teamRepository;
            _mapper = mapper;
            _tasks = _taskRepository.GetList().Select(item => _mapper.Map<Tasks, TasksDTO>(item)).ToList();
            _projects = _projectRepository.GetList().Select(item => _mapper.Map<Project, ProjectDTO>(item)).ToList();
            _users = _userRepository.GetList().Select(item => _mapper.Map<User, UserDTO>(item)).ToList();
            _teams = _teamRepository.GetList().Select(item => _mapper.Map<Team, TeamDTO>(item)).ToList();
        }

        private void GetValue(object sender, BasicDeliverEventArgs arg)
        {
            var value = Encoding.UTF8.GetString(arg.Body);
            _projectHub.Clients.All.SendAsync("GetNotification", value);
            _messageConsumerScope.MessageConsumer.SetActnowledge(arg.DeliveryTag, true);
        }

        public IEnumerable<ResultModelDTO> GetAllFromFile()
        {
            List<ResultModelDTO> result = new List<ResultModelDTO>();
            using (StreamReader sr = new StreamReader("../Worker/Result.txt"))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    result.Add(JsonConvert.DeserializeObject<ResultModelDTO>(line));
                }
            }
            return result;
        }
        public IEnumerable<CountOfTasksInProjectsDTO> GetCountOfTaskInProjectByUserId(int Id)
        {
            _messageProducerScope.MessageProducer.Send("Get Count Of Task in project by user Id");
            var result = from project in _projects
                         join task in _tasks on project.Id equals task.Project_id
                         where task.Performer_id == Id
                         group task by project into ress
                         select new CountOfTasksInProjectsDTO { Project = ress.Key, CountOfTasks = ress.Count() };
            return result.ToList();
        }
        public ProjectInformationDTO GetByProjectId(int Id)
        {
            _messageProducerScope.MessageProducer.Send("Get Information about project by Id");
            var result = (from project in _projects
                          join user in _users on project.Team_id equals user.Team_id
                          where project.Id == Id
                          group user by project into ress
                          select new ProjectInformationDTO
                          {
                              Project = _projects.Where(a => a.Id == Id).FirstOrDefault(),
                              TaskWithLongestDescrition = _tasks.Where(a => a.Project_id == Id).OrderByDescending(a => a.Description.Length).FirstOrDefault(),
                              TaskWithShortestName = _tasks.Where(a => a.Project_id == Id).OrderBy(a => a.Name.Length).FirstOrDefault(),
                              CountOfUser = ress.Count()
                          }).FirstOrDefault();
            return result;
        }
        public UserInformationDTO GetByUserId(int Id)
        {
            _messageProducerScope.MessageProducer.Send("Get Information about user by Id");
            var result = (from user in _users
                          where user.Id == Id
                          select new UserInformationDTO
                          {
                              User = user,
                              MinProject = _projects.Where(a => a.Author_id == Id).OrderByDescending(a => a.Created_at).First(),
                              CountOfTaskInMinProject = _tasks.Where(a => a.Project_id == (_projects.Where(b => b.Author_id == Id).OrderByDescending(b => b.Created_at).First().Id)).Count(),
                              CountOfCanceledOrNotFinishedTasks = _tasks.Where(a => a.Performer_id == Id && (a.State == 2 || a.State == 4)).Count(),
                              LongestTask = _tasks.Where(a => a.Performer_id == Id).OrderByDescending(a => (a.Finished_at - a.Created_at)).First()
                          }).FirstOrDefault();
            return result;
        }
        public IEnumerable<FinishedTasksDTO> GetFinishedTasks(int Id)
        {
            _messageProducerScope.MessageProducer.Send("Get finished tasks");
            var result = _tasks.Where(a => a.Performer_id == Id && a.Finished_at.Year == 2019 && a.State == 3).Select(a => new FinishedTasksDTO { Id = a.Id, Name = a.Name }).ToList();
            return result;
        }
        public IEnumerable<UserWithTasksDTO> GetSortedListByUserAndTeam()
        {
            _messageProducerScope.MessageProducer.Send("Get sorted list with user and team");
            var result = from user in _users
                         join task in _tasks on user.Id equals task.Performer_id
                         orderby user.First_name
                         group task by user into ress
                         select new UserWithTasksDTO { User = ress.Key, Tasks = ress.OrderByDescending(a => a.Name.Length).ToList() };
            return result;
        }
        public IEnumerable<TasksDTO> GetTaskOfUser(int Id)
        {
            _messageProducerScope.MessageProducer.Send("Get tasks of user");
            var result = _tasks.Where(a => a.Performer_id == Id && a.Name.Length < 45).ToList();
            return result;
        }
        public IEnumerable<TeamWithUserDTO> GetTeamWithYoungUser()
        {
            _messageProducerScope.MessageProducer.Send("Get team with young user");
            var result = (from team in _teams
                          join user in _users on team.Id equals user.Team_id
                          orderby user.Registered_at
                          group user by team into ress
                          where ress.All(a => DateTime.Now.Year - a.Birthday.Year >= 12)
                          select new TeamWithUserDTO
                          {
                              Id = ress.Key.Id,
                              Name = ress.Key.Name,
                              Users = ress.ToList()
                          });
            return result;
        }
    }
}
