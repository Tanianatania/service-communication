﻿using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using System.Collections.Generic;
using System.Text;
using BLL.DTO;
using System.Linq;
using SharedQueueService.Interfaces;
using SharedQueueService.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Microsoft.AspNetCore.SignalR;
using BLL.Hubs;

namespace BLL.Services
{
    public class ProjectService : IService<ProjectDTO>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Project> _repository;
        IMessageProducerScope _messageProducerScope;
        IMessageConsumerScope _messageConsumerScope;
        private readonly IHubContext<ProjectHub> _projectHub;

        public ProjectService(IRepository<Project> repository,
             IMessageProducerScopeFactory messageProducerScopeFactory,
             IMessageConsumerScopeFactory messageConsumerScopeFactory,
             IHubContext<ProjectHub> projectHub,
            IMapper mapper)
        {
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSetting
            {
                ExchangeName= "ServerExchange",
                ExchangeType=ExchangeType.Topic,
                QueueName="SendValueQueue",
                RoutingKey="topic.queue"
            });

            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSetting
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponseQueue",
                RoutingKey = "response"
            });
            _messageConsumerScope.MessageConsumer.Received += GetValue;

            _projectHub = projectHub;
            _repository = repository;
            _mapper = mapper;
        }

        private void GetValue(object sender, BasicDeliverEventArgs arg)
        {
            var value = Encoding.UTF8.GetString(arg.Body);
            _projectHub.Clients.All.SendAsync("GetNotification", value);
            _messageConsumerScope.MessageConsumer.SetActnowledge(arg.DeliveryTag, true);
        }

        public void Create(ProjectDTO item)
        {
            _messageProducerScope.MessageProducer.Send("Project creating was triggered");
            var project = _mapper.Map<ProjectDTO, Project>(item);
            _repository.Create(project);
        }

        public void Delete(int id)
        {
            _messageProducerScope.MessageProducer.Send("Project deleting was triggered");
            _repository.Delete(id);
        }

        public ProjectDTO Get(int id)
        {
            _messageProducerScope.MessageProducer.Send("Project getting by Id was triggered");
            return _mapper.Map<Project,ProjectDTO>(_repository.Get(id));
        }

        public IEnumerable<ProjectDTO> GetList()
        {
            _messageProducerScope.MessageProducer.Send("Loading all projects was triggered");
            return _repository.GetList().Select(item=>_mapper.Map<Project,ProjectDTO>(item));
        }

        public void Update(ProjectDTO item)
        {
            _messageProducerScope.MessageProducer.Send("Project updating was triggered");
            var project = _mapper.Map<ProjectDTO, Project>(item);
            _repository.Update(project);
        }
    }
}
